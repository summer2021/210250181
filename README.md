项目名称：DoKit For Android
项目详细方案：
关于导航功能，我们采用对腾讯地图和百度地图的 API  服务来实现，写入程序；
关于启动耗时的技术，我们采用字节码替换，可以参照滴滴开源 DroidAssist 的字节码编辑插件进行修改和完善。
Activity 的启动分为Pause、Launch 和Render 三个步骤，在启动一个新 Activity 时，会先 Pause 前一个正在显示的 Activity，再加载新 Activity，然后开始渲染，直到第一帧渲染成功，Activity 才算启动完毕
可 以 利 用 Logcat 查 看 系 统 输 出 的 Activity 启 动 耗 时 ， 系 统 会 统 计Activity Launch+Render 的时间做为耗时时间，而系统最多允许 Pause 操作超时 500ms，到时见就会自己调用 Pause 完成方法进行后续流程。
可以使用 Hook Instrumentation、Hook Looper、Hook Handler 三种方式实现 AOP 的耗时统计，其中 Hook Looper 方式无法兼容 Android P

项目开发时间计划：
7.01-8.15:
实现针对腾讯以及百度的实时导航功能；
升级 Android Activity 启动耗时的技术方案，采用字节码替换。
8.16-9.30:
升级内置工具的注册方式（配置文件升级为注解）；
基于 Dokit 现有的框架，独立负责并产出一款创造性的工具。
